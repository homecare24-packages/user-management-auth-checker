package ums_middleware

import (
	"fmt"
	"github.com/labstack/echo/v4"
)

func UseUserManagementChecker(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		if err := next(c); err!=nil {
			c.Error(err)
		}
		header := c.Request().Header.Get("Authorization")

		fmt.Println(header)

		return nil
	}
}
